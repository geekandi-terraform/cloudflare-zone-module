resource "cloudflare_zone" "dns_zone" {
  count = var.create_zone == "true" ? 1 : 0
  zone  = var.zone
}
