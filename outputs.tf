output "id" {
  value = cloudflare_zone.dns_zone.*.id
}

output "vanity_name_servers" {
  value = cloudflare_zone.dns_zone.*.vanity_name_servers
}

# does not work
# output "wildcard_proxiable" {
#   value = "${cloudflare_zone.dns_zone.meta.wildcard_proxiable}"
# }

# does not work
# output "phishing_detected" {
#   value = "${cloudflare_zone.dns_zone.meta.phishing_detected}"
# }

output "status" {
  value = cloudflare_zone.dns_zone.*.status
}

output "type" {
  value = cloudflare_zone.dns_zone.*.type
}

output "name_servers" {
  value = cloudflare_zone.dns_zone.*.name_servers
}
