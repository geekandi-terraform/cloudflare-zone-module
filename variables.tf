variable "zone" {
  type = string
}

variable "create_zone" {
  type    = bool
  default = "false"
}
