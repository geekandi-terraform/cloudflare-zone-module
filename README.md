# Cloudflare Zone Module

[Terraform cloudflare_zone](https://www.terraform.io/docs/providers/cloudflare/r/zone.html)

> **Requires Terraform 0.13 or higher**

## usage example

## required variables
`domain` - domain to create a zone for

## optional variables
`create_zone` - if we need to actually create said zone

## outputs
* `id` - The zone ID.
* `vanity_name_servers` - List of Vanity Nameservers (if set).
* **does not work** `meta.wildcard_proxiable` - Indicates whether wildcard DNS records can receive Cloudflare security and performance features.
* **does not work** `meta.phishing_detected` - Indicates if URLs on the zone have been identified as hosting phishing content.
* `status` - Status of the zone. Valid values: active, pending, initializing, moved, deleted, deactivated
* `type` - A full zone implies that DNS is hosted with Cloudflare. A partial zone is typically a partner-hosted zone or a CNAME setup. Valid values: full, partial
* `name_servers` - Cloudflare-assigned name servers. This is only populated for zones that use Cloudflare DNS.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | 3.7.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_zone.dns_zone](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/zone) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_zone"></a> [create\_zone](#input\_create\_zone) | n/a | `bool` | `"false"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | n/a |
| <a name="output_name_servers"></a> [name\_servers](#output\_name\_servers) | n/a |
| <a name="output_status"></a> [status](#output\_status) | n/a |
| <a name="output_type"></a> [type](#output\_type) | n/a |
| <a name="output_vanity_name_servers"></a> [vanity\_name\_servers](#output\_vanity\_name\_servers) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
